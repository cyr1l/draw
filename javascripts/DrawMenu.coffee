define ['jquery', 'backbone', 'underscore','D3'], ($,Backbone, _,D3)->
  console.log('Menu Component Loaded!')

  class DrawMenuView extends Backbone.View
    el: $('.toolbar')
    events:
      'click .export': "renderExport"
      'click .undo': 'undo'
      'click .redo': 'redo'
      'click .save': 'save'
    undo: ->
      @trigger('undo')
    redo: ->
      @trigger('redo')
    renderExport: (e) ->
      css = """
        <style type="text/css" >
          <![CDATA[
            path.link {
              fill: none;
              stroke: #000;
              stroke-width: 4px;
              cursor: default;
            }
            g.node rect.bg {
              fill: none;
              stroke: #cacaca;
            }
            g.node.hover rect.bg {
              stroke: #838383;
            }
            path.link {
              cursor: pointer;
            }
            path.link.dragline {
              pointer-events: none;
            }
            path.link {
              stroke-dasharray: 10px 0px;
            }
            path.link.selected {
              stroke-dasharray: 10px 5px;
            }
            g.group g text.id {
              cursor: -webkit-grab !important;
              fill: #fff;
            }
            g g.active text.id {
              cursor: -webkit-grabbing !important;
            }
            text.id {
              text-anchor: middle;
              font-size: 10px;
              font-weight: 100;
            }
          ]]>
        </style>

      """
      html = D3.select("svg")
      .attr("version", 1.1)
      .attr("xmlns", "http://www.w3.org/2000/svg")
      .node().parentNode.innerHTML
      html = html.replace 'xmlns="http://www.w3.org/2000/svg">','xmlns="http://www.w3.org/2000/svg">'+css
      imgsrc = 'data:image/svg+xml;base64,'+ btoa(html)
      img = '<img src="'+imgsrc+'">'
      D3.select("#svgdataurl").html(img)
      canvas = document.querySelector("canvas")
      context = canvas.getContext("2d")
      image = new Image
      image.src = imgsrc
      image.onload = ->
        context.drawImage(image, 0, 0)
        canvasdata = canvas.toDataURL("image/png")
        pngimg = '<img src="'+canvasdata+'">'
        D3.select("#pngdataurl").html(pngimg)
        a = document.createElement("a")
        a.download = "export.png"
        a.href = canvasdata
        a.click()
    save: ->
      @trigger('save')
  DrawMenuView: DrawMenuView
