define ['jquery',"backbone",'DrawSidebar','DrawMenu','DrawCanvas','config'],($,Backbone,DrawSidebar,DrawMenu,DrawCanvas,config)->
  initialize: ->
    # drawMenu : new DrawMenu
    sidebarItems = config.sidebarItems
    drawSidebar = new DrawSidebar.DrawSidebarView {collection: new DrawSidebar.DrawSidebarCollection sidebarItems}
    drawCanvas = new DrawCanvas.DrawCanvasView()
    drawMenu = new DrawMenu.DrawMenuView()

    `data=
    {"nodes":[{"x":427,"y":81,"type":"windows","image":"http://cyrilis.com/draw/images/windows.png","cid":"c32"},{"x":261,"y":172,"type":"ubuntu","image":"http://cyrilis.com/draw/images/ubuntu.png","cid":"c33"},{"x":610,"y":168,"type":"amazon","image":"http://cyrilis.com/draw/images/amz.png","cid":"c34"},{"x":260,"y":344,"type":"redhat","image":"http://cyrilis.com/draw/images/redhat.png","cid":"c35"},{"x":426,"y":457,"type":"main","image":"http://cyrilis.com/draw/images/icon.png","cid":"c36"},{"x":610,"y":347,"type":"suse","image":"http://cyrilis.com/draw/images/suse.png","cid":"c37"}],"links":[{"source":"c32","target":"c33"},{"source":"c33","target":"c35"},{"source":"c35","target":"c36"},{"source":"c36","target":"c37"},{"source":"c34","target":"c37"},{"source":"c34","target":"c32"},{"source":"c32","target":"c36"},{"source":"c36","target":"c34"},{"source":"c37","target":"c33"},{"source":"c33","target":"c34"},{"source":"c35","target":"c32"},{"source":"c35","target":"c37"},{"source":"c32","target":"c37"},{"source":"c33","target":"c36"},{"source":"c34","target":"c35"}]}
    `
    #  drawCanvas.collection.sync()
    drawCanvas.links.add(data.links)
    drawCanvas.collection.add(data.nodes)
    drawSidebar.on 'appendnode',(e)->
      drawCanvas.collection.add [e.detail]
    drawMenu.on 'undo', ->
      drawCanvas.undo()
    drawMenu.on 'redo', ->
      drawCanvas.redo()
    drawMenu.on 'save', ->
      drawCanvas.save()