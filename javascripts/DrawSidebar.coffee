define ['jquery', 'backbone', 'underscore','config'], ($,Backbone, _, config)->
  #Todo: Delete  init data


  types=config.nodeType
  # Init Draw Sidebar Model
  class DrawSidebarModel extends Backbone.Model
    sync: ->
      false
    default:
      color: 'blue'
      name: "Default"
      active: false
      type: 'main'
      image: "./images/icon.png"
    initialize: ->
      @set 'cid', @cid unless @get 'cid'
      @set @default unless @get "name"
      @set('image', types[@get('type')])


  # sidebar Collection
  class DrawSidebarCollection extends Backbone.Collection
    model: DrawSidebarModel

  # Sidebar Item View
  class DrawSidebarItemView extends Backbone.View
    el_tag : 'li'
    template: _.template($('#sidebar').html())
    initialize: =>
      @model.view = @
    render: =>
      @template(@model.toJSON())

  # Sidebar View
  class DrawSidebarView extends Backbone.View
    el: $('ul.sidebar')
    events:
      'mousedown li' : 'overlay',
    model: DrawSidebarModel
    overlay: (e) =>
      ele = if  e.target.nodeName =="li" then  e.target else e.target.parentElement
      targetModel = @.collection.get $(ele).attr('id')
      targetType = targetModel.toJSON().type
      view = @
      $("<div id='overlay'><img id='copy' src='"+config.nodeType[targetType]+"'>")
      .appendTo('body')
      .find('#copy')
      .css
        top: e.clientY - 29
        left: e.clientX - 30
      .end()
      .on 'mouseup', (e)=>
          svg = $('svg')
          body = $('body')
          data =
            x:e.clientX - svg.offset().left
            y:e.clientY - svg.offset().top  + body.scrollTop()
            type: targetType
          if 0< data.x < svg.width() and 0< data.y < svg.height()
            view.trigger('appendnode', {
              detail: data
            })
          $('#overlay').remove()
      .on 'mousemove', (e) ->
          $("#copy").css
            top: e.clientY - 29
            left: e.clientX - 30
    initialize: =>
      $(@el).html("")
      @collection.each(@addOne)
      @collection.bind('add', @addOne)
    addOne:  (model) =>
      $(@el).append(new DrawSidebarItemView({model: model}).render())
    render : ->
      @collection.add(new @model())

  DrawSidebarView: DrawSidebarView
  DrawSidebarCollection: DrawSidebarCollection