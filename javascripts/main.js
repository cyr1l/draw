/**
 * Created by Cyril on 14-3-20.
 */
"use strict";

var paths = {
    backbone: './lib/backbone',
    jquery:'./lib/jquery-1.9.1.min',
    underscore: './lib/underscore-min',
    handlebars: './lib/handlebars.runtime',
    helpers: './util/helpers',
    D3: './lib/d3.min'
};

var shim = {
    handlebars: {
        exports: 'Handlebars'
    },
    Backbone: {
        deps: ['underscore', 'jquery'],
        exports: 'Backbone'
    },
    underscore: {
        exports: '_'
    }
};

// set up require.js
require.config({
    baseUrl: './javascripts',
    paths: paths,
    shim: shim,
    urlArgs: "bust=" + (new Date()).getTime()
});

require(["app"],function(App){
    // initialize our application
    App.initialize();
});