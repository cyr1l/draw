define ['backbone', 'underscore', 'D3', 'config'], (Backbone, _, D3 , config)->
  console.log('Canvas Component Loaded!')
  defaultWidth = 1000
  defaultHeight = 800


  #
  # Canvas Model
  #

  class DrawCanvasNodeModel extends Backbone.Model
    defaults :
      type: "main"
      x: null
      y: null

    initialize: =>
      @set 'x', Math.random()*defaultWidth unless @get('x')
      @set 'y', Math.random()*defaultHeight unless @get('y')
      data = config.nodeType
      @attributes['image'] = @get('image')|| data[@get("type")||@defaults['type']]
      @set 'cid' , @cid unless @get('cid')


  class DrawCanvasLinkModel extends Backbone.Model
    defaults:
      source: ""
      target: ""
  #
  # Canvas Collection
  #

  class DrawCanvasNodeCollection extends  Backbone.Collection
    model: DrawCanvasNodeModel
#    sync: ->
    


  class DrawCanvasLinkCollection extends Backbone.Collection
    model: DrawCanvasLinkModel

  #
  # Canvas View Init Function
  #

  renderCanvas = ->
    svg = D3.select('.canvas').append('svg')
    .attr("width", defaultWidth)
    .attr("height", defaultHeight)
    .attr('xmlns:xmlns:xlink',"http://www.w3.org/1999/xlink")
    .attr("tabindex", 1)
    svg.append('svg:defs').append('svg:marker')
    .attr('id', 'end-arrow')
    .attr('viewBox', '0 -5 10 10')
    .attr('refX', 4)
    .attr('markerWidth', 3)
    .attr('markerHeight', 3)
    .attr('orient', 'auto')
    .append('svg:path')
    .attr('d', 'M0,-5L10,0L0,5')
    .attr('fill', 'rgba(0, 151, 255, 1)')
    svg.append('svg:defs').append('svg:marker')
    .attr('id', 'start-arrow')
    .attr('viewBox', '0 -5 10 10')
    .attr('refX', 4)
    .attr('markerWidth', 3)
    .attr('markerHeight', 3)
    .attr('orient', 'auto')
    .append('svg:path')
    .attr('d', 'M10,-5L0,0L10,5')
    .attr('fill', 'rgba(0, 151, 255, 1)')
    path = svg.append('svg:g').classed('pathGroup',true).selectAll('g')


  #
  # Canvas View
  #

  class DrawCanvasView extends  Backbone.View
    #model: DrawCanvasModel
    initialize: =>
      renderCanvas()
      @bindClick()
      @collection= new DrawCanvasNodeCollection()
      @links = new DrawCanvasLinkCollection()
      @collection.on 'add', @render
      @links.on 'add', @render
      console.log "Initialize!"
    undo: =>
      console.log "undoing.... "
      if @undolist.canUndo()
        undos = @undolist.prev()
        @collection.reset(undos.node)
        @links.reset undos.link
        @render()
      else
        console.log('Can\'t undo any more')
        alert "Can't undo anymore!"
    redo: =>
      console.log "redoing.... "
      if @undolist.canRedo()
        redos = @undolist.next()
        if redos.node
          @collection.reset redos.node
          @links.reset redos.link
          @render()
      else
        console.log 'Can\'t Redo anymore!'
        alert 'Can\'t Redo anymore!'
    undolist:
      nodes: []
      links: []
      mark: 0
      now: ->
        node: @nodes[@mark]
        link: @links[@mark]
      prev: ->
        @canUndo()&&@mark--
        @now()
      next: ->
        @canRedo()&&@mark++
        @now()
      canRedo: ->
        @mark < @nodes.length&&@mark>=0
      canUndo: ->
        @nodes.length>0 && @mark>0

    # trigger changed to run updateUndolist
    updateUndolist: =>

      if @undolist.nodes.length>20
        @undolist.nodes.shift()
        @undolist.links.shift()
        @undolist.mark = @undolist.nodes.length-1
      cNode = @undolist.now().node
      cLink = @undolist.now().link
      nNode = @collection.toJSON()
      nLink = @links.toJSON()
      if (_.isEqual _.pluck(cNode,'x'), _.pluck(nNode,'x')) and (_.isEqual _.pluck(cNode,'y'), _.pluck(nNode,'y')) and (_.isEqual(cLink,nLink))
        console.log 'DO NOT Update'
        return false
      console.log 'updated Undolist'
      @undolist.nodes.push @collection.toJSON()
      @undolist.links.push @links.toJSON()
      @undolist.mark = @undolist.nodes.length-1
    render: =>
      svg = D3.select('svg')
      oldNodeGroup = svg.selectAll('g.group');
      oldPathGroup = svg.selectAll('g.pathGroup').selectAll('path')
      oldNodeGroup.remove()
      oldPathGroup.remove()
      @drawLine(@)
      g = svg.append('svg:g').classed('group',true).selectAll("g")
      g = g.data @collection.toJSON()
      gg = g.enter().append("svg:g")
      .classed('node', true)
      .attr 'transform' ,(d)->
        "translate(#{d.x}, #{d.y})"
      .call(@drag())
      gg.on('mouseover',@mouseover)
      gg.on('mouseleave',@mouseleave)
      gg.append('svg:rect')
      .attr 'class' , 'bg'
      .attr 'x', -32
      .attr 'y', -32
      .attr 'width', 64
      .attr 'height', 62
      .attr 'rx', 5
      .attr 'rx', 5
      gg.append('svg:image')
        .attr 'xlink:href',(d)->
            return d.image
        .attr  "height", 57
        .attr 'width', 60
        .attr 'x', -30
        .attr 'y', -29
      gg.append('svg:text')
      .attr('x', 0)
      .attr('y', -15)
      .attr('class', 'id')
      .text (d)=>
        "Node-"+ d.cid
    drawLine: (view)=>
      if not view.collection.toJSON().length
        return false
      svg = D3.select('svg');
      path = svg.selectAll('g.pathGroup').selectAll('g')
      path = path.data @links.toJSON()
      path.enter().append('svg:path')
      .attr('class', 'link')
      .classed 'selected', (d)->
          d.selected
      .style 'marker-start',(d) ->
          if d.left then 'url(#start-arrow)' else ''
      .style 'marker-end', (d) ->
          if d.right then 'url(#end-arrow)' else ''
      .style('stroke', 'rgba(0, 151, 255, 1)')
      .attr 'd', (d) ->
          source = _.find _.pluck(view.collection.models,"attributes"), (e) ->
            e.cid == d.source
          target = _.find _.pluck(view.collection.models,"attributes"), (e) ->
            e.cid == d.target
          "M #{source.x} , #{source.y} L#{target.x},#{target.y}"


    # Bind Click and Delete Event
    bindClick: =>
      document.querySelector('svg').addEventListener 'click', (e)=>
        D3.select('.selected').classed('selected',false)
        if(e.target && e.target.nodeName =="path")
          D3.select(e.target).classed('selected',true)
        if(e.target && e.target.nodeName =="image"||e.target.nodeName == 'text'||Array::indexOf(e.target.parentElement.classList,'node')>-1)
          D3.select(e.target.parentElement).classed('selected',true)
      document.addEventListener 'keyup', (e)=>
        code = e.keyCode || e.which;
        if code == 46
          data =  if D3.selectAll('.selected').size() then D3.selectAll('.selected').datum() else {}
          if data.type
            selectedModel = @collection.where(data)
            @collection.remove(selectedModel)
            @links.remove @links.where({target: data.cid})
            @links.remove @links.where({source: data.cid})
          if data.source
            selectedModel = @links.where(data)
            @links.remove(selectedModel)
        @render()
        @updateUndolist()
    mouseover: ->
      D3.select('g.group g.hover')
      .classed 'hover', false
      D3.select this
      .classed 'hover',true
    mouseleave : ->
      D3.select this
      .classed 'hover', false

    drag : =>
      view = @
      collection = @collection
      canDrag = false
      dragingline = null
      sourceElement = null
      drawline = @drawLine
      drawNewLine = (source)->
        dragingline?.remove()
        data = D3.select(source).datum()
        target = D3.event
        path = D3.selectAll('g.pathGroup').append('svg:path')
        path.attr 'class', 'link'
        .classed 'selected', true
        .style 'marker-start', 'url(#start-arrow)'
        .style 'marker-end', 'url(#end-arrow)'
        .style 'stroke' , 'rgba(0, 151, 255, 1)'
        .attr 'd', ->
          "M #{target.x} , #{target.y} L#{data.x},#{data.y}"
        dragingline = path
      mousedown = (d) ->
          view.collection.trigger('changed',{updated: true})
          if  D3.event.sourceEvent.target.tagName=='text'
            canDrag = true
            D3.select(this).classed 'active',true
          else
            canDrag = false
            sourceElement = this
      mousemove = (d)->
        if not canDrag
          drawNewLine(sourceElement)
        else
          path = D3.selectAll(document.querySelector('g.pathGroup').childNodes)
          D3.select(this)
          .attr "transform",  (d) ->
              path.remove()
              drawline(view)
              d.x = D3.event.x
              d.y = D3.event.y
              "translate(#{D3.event.x}, #{D3.event.y})"
          path.remove()
          view.collection.findWhere({cid: d.cid}).set({x:d.x,y:d.y})
      mouseup = (e) ->
        D3.select(this).classed 'active', false
        canDrag = false
        dragingline?.remove()
        targetElement = D3.event.sourceEvent.target
        mouseUpElement =
          if  targetElement.nodeName== 'image' and Array::indexOf.call(targetElement.parentNode.classList,"node")>-1
          then targetElement.parentElement
          else if Array::indexOf.call(targetElement.classList,"node")>-1
          then targetElement
          else null
        if mouseUpElement and (mouseUpElement isnt this)
          source = D3.select(sourceElement).datum().cid
          target = D3.select(mouseUpElement).datum().cid
          if _.find(view.links.toJSON(),(e)-> e.source == source && e.target == target) || _.find(view.links.toJSON(),(e)-> e.source == target && e.target == source)
            console.log 'You\'v Got Linked already!'
            return
          else
            console.info "%c(\\_/)\n(O_O)\n(> <)  ♩ ♪ ♫ I Drew a line, I drew a line for you~~~ ♫ ♪ ♩ ---James Blunt.", "font-size: 14px;color:#0099ff;font-family: 'Consolas';"
            view.links.add([{source: source, target: target}])
#            drawline(view)
        else
          console.log("Target not found, sir.")
          false
        sourceElement = null
        view.updateUndolist()

      D3.behavior.drag()
      .origin (d) -> d
      .on "drag" , mousemove
      .on 'dragstart' , mousedown
      .on 'dragend' , mouseup
    save:->
      json =
        nodes: @collection.toJSON()
        links: @links.toJSON()
      console.log json
      alert JSON.stringify(json)


  DrawCanvasView : DrawCanvasView