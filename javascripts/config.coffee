define [],->
  rootPath = "http://cyrilis.com"
  nodeType =
    main: rootPath + "/draw/images/icon.png"
    amazon: rootPath + "/draw/images/amz.png"
    suse: rootPath + "/draw/images/suse.png"
    redhat: rootPath + "/draw/images/redhat.png"
    ubuntu: rootPath + "/draw/images/ubuntu.png"
    windows: rootPath + "/draw/images/windows.png"
    blank: rootPath + "/draw/images/blank.png"

  sidebarItems = [
    {
      name: "main"
      color: "red"
      type: 'main'
    }
    {
      name: "amz"
      color: "blue"
      type: 'amazon'
    }
    {
      name: "redhat"
      color: "blue"
      type: 'redhat'
    }
    {
      name: "suse"
      color: "blue"
      type: 'suse'
    }
    {
      name: "ubuntu"
      color: "blue"
      type: 'ubuntu'
    }
    {
      name: "windows"
      color: "blue"
      type: 'windows'
    }
    {
      name: "blank"
      color: "blue"
      type: 'blank'
    }
    {
      name: "name2"
      color: "blue"
      type: 'main'
    }
    {
      name: "name2"
      color: "blue"
      type: 'main'
    }
  ]

  nodeType: nodeType
  sidebarItems: sidebarItems